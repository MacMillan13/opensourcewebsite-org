[![first-timers-only](https://img.shields.io/badge/first--timers--only-friendly-blue.svg?style=flat-square)](https://www.firsttimersonly.com) [![opensource.guide](https://img.shields.io/badge/opensource.guide-friendly-blue.svg?style=flat-square)](https://opensource.guide)

[![pipeline status](https://gitlab.com/opensourcewebsite-org/opensourcewebsite-org/badges/master/pipeline.svg)](https://gitlab.com/opensourcewebsite-org/opensourcewebsite-org/commits/master) [![coverage report](https://gitlab.com/opensourcewebsite-org/opensourcewebsite-org/badges/master/coverage.svg)](https://gitlab.com/opensourcewebsite-org/opensourcewebsite-org/commits/master)


# OpenSourceWebsite

OpenSourceWebsite (OSW) is an open source website for the world's largest online social community, and is available for free on [Live Website](https://opensourcewebsite.org)

## Minimum technical requirements

- ![PHP 7](https://img.shields.io/badge/Powered_by-PHP-green.svg?style=flat) - PHP 7.0
- ![Yii 2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat) - Yii 2.0.14
- ![MySQL 5](https://img.shields.io/badge/Powered_by-MySQL-green.svg?style=flat) - MySQL 5.7

## Code of conduct

This project and everyone participating in it is governed by the [Code of Conduct](CODE_OF_CONDUCT.md). By participating, you are expected to uphold this code. Please report unacceptable behavior to [hello@opensourcewebsite.org](mailto:hello@opensourcewebsite.org).

## Contributing

Please read through our [contributing guidelines](CONTRIBUTING.md).

## License

The code is Open Source and available freely under the [MIT license](LICENSE.md), powered by [an excellent community](https://gitlab.com/opensourcewebsite-org/opensourcewebsite-org/graphs/master).

## Support

If you're looking for support there are a lot of options, check out:

- [API Documentation](API Documentation)
- [Wiki](https://gitlab.com/opensourcewebsite-org/opensourcewebsite-org/wikis/home)
- [Chat](Chat)
